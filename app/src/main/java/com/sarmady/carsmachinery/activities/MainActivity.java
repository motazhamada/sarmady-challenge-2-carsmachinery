package com.sarmady.carsmachinery.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.fragments.HomeFragment;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

//    @BindView(R.id.bnv_cars_main)
//    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        // Check that the activity is using the layout version with the fragment_container FrameLayout
        if(findViewById(R.id.fl_container) != null)
        {
            // if we are being restored from a previous state, then we dont need to do anything and should
            // return or else we could end up with overlapping fragments.
            if(savedInstanceState != null)
                return;
            // Create an instance of editorFrag
            HomeFragment firstFrag = new HomeFragment();
            // add fragment to the fragment container layout
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, firstFrag).commit();
           }
    }


}