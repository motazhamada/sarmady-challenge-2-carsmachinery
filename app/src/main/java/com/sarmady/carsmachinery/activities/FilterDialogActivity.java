package com.sarmady.carsmachinery.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.sarmady.carsmachinery.R;

import java.util.ArrayList;

public class FilterDialogActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private ArrayList<String> mCarModels;
    private Spinner spinner;
    private Intent intent;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_dialog);

        intent = getIntent();
        mCarModels = (ArrayList<String>) getIntent().getSerializableExtra("mCarModels");
        spinner = findViewById(R.id.spinner_filter);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mCarModels);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.select_dialog_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (count >= 1) {
            String model = spinner.getSelectedItem().toString();
            intent.putExtra("model", model);
            setResult(RESULT_OK, intent);
            finish();
        }
        count++;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}
