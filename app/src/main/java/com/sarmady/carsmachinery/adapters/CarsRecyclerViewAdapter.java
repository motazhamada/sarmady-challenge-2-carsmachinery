package com.sarmady.carsmachinery.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.classes.CarItem;
import com.sarmady.carsmachinery.classes.CarsList;
import com.sarmady.carsmachinery.utilities.SavePrefs;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CarItem> mData;
    private CarsList mFavoriteData;
    private LayoutInflater mInflater;

    private Context mContext;
    private SavePrefs savePrefs;

    private String mImageHeight = "120", mImageWidth = "120";

    private Handler handler = new Handler();

    private boolean LinearLayout;

    // data is passed into the constructor
    public CarsRecyclerViewAdapter(Context context, List<CarItem> data, boolean LinearLayout) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.LinearLayout = LinearLayout;
        //noinspection unchecked
        savePrefs = new SavePrefs(mContext, CarsList.class);
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (LinearLayout)
            view = mInflater.inflate(R.layout.item_car_linear, parent, false);
        else
            view = mInflater.inflate(R.layout.item_car_grid, parent, false);
        return new ViewHolder(view);
    }

    public void add(CarItem item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void add(int position, CarItem item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void addAll(List<CarItem> items) {
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }

    public void clearAll() {
        handler.removeCallbacksAndMessages(null);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final CarItem mCarItem = mData.get(position);
        try {
            final ViewHolder viewHolder = (ViewHolder) holder;
            loadTextAndTimer(mCarItem, viewHolder);

            loadCarImage(mCarItem, viewHolder);


            loadAndClickListenerFavoriteButton(mCarItem, viewHolder);
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), "CarsRecyclerViewAdapter 1 = " + String.valueOf(e));
        }
    }

    private void loadAndClickListenerFavoriteButton(CarItem mCarItem, ViewHolder viewHolder) {
        if (Objects.equals(Check_Favorite(mCarItem), false)) {
            viewHolder.mFavoriteImageButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
        } else {
            viewHolder.mFavoriteImageButton.setImageResource(R.drawable.ic_favorite_red_24dp);
        }


        viewHolder.mFavoriteImageButton.setOnClickListener(v -> {
            if (Objects.equals(Favorite(mCarItem), false)) {
                viewHolder.mFavoriteImageButton.setImageResource(R.drawable.ic_favorite_red_24dp);
            } else {
                viewHolder.mFavoriteImageButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
            }
        });
    }

    private void loadCarImage(CarItem mCarItem, ViewHolder viewHolder) {
        final ViewTreeObserver observer = viewHolder.mCarImageView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(
                () -> {
                    if (viewHolder.mCarImageView.getHeight() != 0 || viewHolder.mCarImageView.getWidth() != 0) {
                        mImageHeight = String.valueOf(viewHolder.mCarImageView.getHeight());
                        mImageWidth = String.valueOf(viewHolder.mCarImageView.getWidth());
                    }
                });

        String mImageUrl = mCarItem.getImage();
        mImageUrl = mImageUrl.replace("[h]", mImageHeight);
        mImageUrl = mImageUrl.replace("[w]", mImageWidth);

        Picasso.get()
                .load(mImageUrl)
                .fit()
                .placeholder(R.drawable.ic_directions_car_black_24dp)
                .error(R.drawable.ic_broken_image_black_24dp)
                .into(viewHolder.mCarImageView);
    }

    private void loadTextAndTimer(CarItem mCarItem, ViewHolder viewHolder) {
        if (isLinearLayout())
            viewHolder.mCarModelTextView.setText(String.format("%s %s", mCarItem.getMakeEn(), mCarItem.getModelEn()));
        else
            viewHolder.mCarModelTextView.setText(String.format("%s\n%s", mCarItem.getMakeEn(), mCarItem.getModelEn()));
        viewHolder.mCarPriceTextView.setText(String.valueOf(mCarItem.getAuctionInfo().getCurrentPrice()));
        viewHolder.mCarPriceCurrencyTextView.setText(mCarItem.getAuctionInfo().getCurrencyEn());
        viewHolder.mCarLotTextView.setText(String.valueOf(mCarItem.getAuctionInfo().getLot()));
        viewHolder.mCarBidSTextView.setText(String.valueOf(mCarItem.getAuctionInfo().getBids()));

        viewHolder.mCarTimeLeftTextView.setText(String.valueOf(mCarItem.getAuctionInfo().getEndDate()));


        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();
        handler.removeCallbacks(viewHolder.customTimerRunnable);
        viewHolder.customTimerRunnable.holder = viewHolder.mCarTimeLeftTextView;
        viewHolder.customTimerRunnable.millisUntilFinished = 1000 * mCarItem.getAuctionInfo().getEndDate() + mGMTOffset;

        handler.postDelayed(viewHolder.customTimerRunnable, 100);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public CarItem getItem(int id) {
        return mData.get(id);
    }

    @SuppressWarnings("unchecked")
    private boolean Favorite(CarItem carItem) {
        try {
            mFavoriteData = new CarsList();
            if (Objects.equals(savePrefs.load(), null)) {
                Add_Favorite(carItem);
                return false;
            } else {
                mFavoriteData = ((CarsList) savePrefs.load());
                ArrayList<CarItem> tempList = new ArrayList<>(mFavoriteData.getCarItems());
                for (int i = 0; i < tempList.size(); i++) {
                    if (tempList.get(i).getCarID() == carItem.getCarID()) {
                        tempList.remove(i);
                        mFavoriteData.setCarItems(tempList);
                        savePrefs.clear();
                        savePrefs.save(mFavoriteData);
                        return true;
                    }
                }
                Add_Favorite(carItem);
                return false;
            }
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), "CarsRecyclerViewAdapter 2 = " + String.valueOf(e));
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private void Add_Favorite(CarItem carItem) {
        try {
            mFavoriteData = new CarsList();
            if (Objects.equals(savePrefs.load(), null)) {
                ArrayList<CarItem> tempList = new ArrayList<>();
                tempList.add(carItem);
                mFavoriteData.setCarItems(tempList);
            } else {
                mFavoriteData = ((CarsList) savePrefs.load());
                ArrayList<CarItem> tempList = new ArrayList<>(mFavoriteData.getCarItems());
                tempList.add(carItem);
                mFavoriteData.setCarItems(tempList);
            }
            savePrefs.clear();
            savePrefs.save(mFavoriteData);
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), "CarsRecyclerViewAdapter 3 = " + String.valueOf(e));
        }
    }

    private boolean Check_Favorite(CarItem carItem) {
        try {
            mFavoriteData = new CarsList();
            if (Objects.equals(savePrefs.load(), null)) {
                return false;
            } else {
                mFavoriteData = ((CarsList) savePrefs.load());
                ArrayList<CarItem> tempList = new ArrayList<>(mFavoriteData.getCarItems());
                for (int i = 0; i < tempList.size(); i++) {
                    if (tempList.get(i).getCarID() == carItem.getCarID()) {
                        return true;
                    }
                }
                return false;
            }
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), "CarsRecyclerViewAdapter 4 = " + String.valueOf(e));
        }
        return false;
    }

    public boolean isLinearLayout() {
        return LinearLayout;
    }

    public void setLinearLayout(boolean linearLayout) {
        LinearLayout = linearLayout;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ibtn_car_favorite)
        ImageButton mFavoriteImageButton;
        @BindView(R.id.iv_car)
        ImageView mCarImageView;
        @BindView(R.id.tv_car_model)
        TextView mCarModelTextView;
        @BindView(R.id.tv_car_price)
        TextView mCarPriceTextView;
        @BindView(R.id.tv_car_price_currency)
        TextView mCarPriceCurrencyTextView;
        @BindView(R.id.tv_car_lot)
        TextView mCarLotTextView;
        @BindView(R.id.tv_car_bids)
        TextView mCarBidSTextView;
        @BindView(R.id.tv_car_time_left)
        TextView mCarTimeLeftTextView;

        CustomTimerRunnable customTimerRunnable;

        ViewHolder(View v1) {
            super(v1);
            ButterKnife.bind(this, v1);
            customTimerRunnable = new CustomTimerRunnable(handler, mCarTimeLeftTextView, 1000);
        }

    }


}