package com.sarmady.carsmachinery.adapters;

import android.graphics.Color;
import android.os.Handler;
import android.widget.TextView;


public class CustomTimerRunnable implements Runnable {

    public long millisUntilFinished;
    public TextView holder;
    public int position;
    Handler handler;

    public CustomTimerRunnable(Handler handler, TextView holder, long millisUntilFinished) {
        this.handler = handler;
        this.holder = holder;
        this.millisUntilFinished = millisUntilFinished;
    }

    @Override
    public void run() {
        /* do what you need to do */
        long seconds = millisUntilFinished / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        String time = String.format("%02d:%02d:%02d", hours, minutes % 60, seconds % 60);
        holder.setText(time);


        millisUntilFinished -= 1000;

        if (millisUntilFinished <= 300000) {
            holder.setTextColor(Color.RED);
        } else {
            holder.setTextColor(Color.BLACK);
        }
        /* and here comes the "trick" */
        handler.postDelayed(this, 1000);
    }

}
