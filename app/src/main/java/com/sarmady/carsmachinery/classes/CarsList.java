package com.sarmady.carsmachinery.classes;

import java.util.ArrayList;

public class CarsList {
    private ArrayList<CarItem> carItems;

    public ArrayList<CarItem> getCarItems() {
        return carItems;
    }

    public void setCarItems(ArrayList<CarItem> carItems) {
        this.carItems = carItems;
    }
}
