package com.sarmady.carsmachinery.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.activities.FilterDialogActivity;
import com.sarmady.carsmachinery.activities.HomeActivity;
import com.sarmady.carsmachinery.adapters.CarsRecyclerViewAdapter;
import com.sarmady.carsmachinery.classes.ApiResponse;
import com.sarmady.carsmachinery.classes.CarItem;
import com.sarmady.carsmachinery.classes.CarsList;
import com.sarmady.carsmachinery.services.RequestListener;
import com.sarmady.carsmachinery.services.ServiceFactory;
import com.sarmady.carsmachinery.utilities.SavePrefs;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment implements RequestListener {


    public static final int REQUEST_CODE_FILTER_SPINNER = 1;
    public static final String PROMPT_STRING_FILTER_SPINNER = "---";
    @BindView(R.id.rv_cars_main)
    RecyclerView mCarsRecyclerView;
    @BindView(R.id.pb_cars_main)
    ProgressBar mCarsProgressBar;
    @BindView(R.id.srl_cars_main)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tb_home)
    Toolbar mToolbar;
    @BindView(R.id.btn_filter)
    Button mFilterButton;
    @BindView(R.id.btn_sort)
    Button mSortButton;
    @BindView(R.id.btn_grid)
    Button mGridButton;
    private List<CarItem> mCarItems, mCarItemsFiltered;
    private ArrayList<String> mCarModels;
    private CarsRecyclerViewAdapter mCarsRecyclerViewAdapter;
    private CarsList mFavoriteData;
    private ApiResponse mApiResponse;
    private SavePrefs savePrefs;
    private String mUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            initialize();
            makeGetRequest();
            Listeners();
        } catch (Exception error) {
            Log.e(getString(R.string.TAG), "HomeFragment 1: " + error.toString());
        }
    }

    private void Listeners() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            makeGetRequest();
            swipeRefreshLayout.setRefreshing(false);
        });
        mFilterButton.setOnClickListener(v -> Filter());
        mSortButton.setOnClickListener(v -> Sort());
        mGridButton.setOnClickListener(v -> GridLinear());
    }

    private void Filter() {
        if (mCarModels.size() > 0) {
            Intent intent = new Intent(getContext(), FilterDialogActivity.class);
            intent.putExtra("mCarModels", mCarModels);
            startActivityForResult(intent, REQUEST_CODE_FILTER_SPINNER);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data.getExtras().containsKey("model")) {
            if (!data.getExtras().getString("model").equals(PROMPT_STRING_FILTER_SPINNER)) {
                mCarItemsFiltered = new ArrayList<>();
                String model = data.getExtras().getString("model");
                for (int i = 0; i < mCarItems.size(); i++) {
                    if (mCarItems.get(i).getModelEn().equals(model)) {
                        mCarItemsFiltered.add(mCarItems.get(i));
                    }
                }
                mCarsRecyclerViewAdapter = new CarsRecyclerViewAdapter(getContext(), mCarItemsFiltered, mCarsRecyclerViewAdapter.isLinearLayout());
                mCarsRecyclerView.setAdapter(mCarsRecyclerViewAdapter);
                mCarsRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    private void Sort() {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getContext(), mSortButton);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.menu_sort, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_sort_end_date:
                    item.setChecked(true);
                    mSortBy("end_date");
                    mCarsRecyclerViewAdapter.notifyDataSetChanged();
                    break;
                case R.id.action_sort_price:
                    item.setChecked(true);
                    mSortBy("price");
                    mCarsRecyclerViewAdapter.notifyDataSetChanged();
                    break;
                case R.id.action_sort_year:
                    item.setChecked(true);
                    mSortBy("year");
                    mCarsRecyclerViewAdapter.notifyDataSetChanged();
                    break;
            }
            return true;
        });

        popup.show(); //showing popup menu
    }

    private void GridLinear() {
        if (mCarsRecyclerViewAdapter.isLinearLayout()) {
            mGridButton.setText("Grid");
            mGridButton.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_view_grid_black_24dp, 0, 0, 0);
            setRecyclerViewLayoutManager("grid");
        } else {
            mGridButton.setText("List");
            mGridButton.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_dehaze_black_24dp, 0, 0, 0);
            setRecyclerViewLayoutManager("linear");
        }
    }

    private void mSortBy(String sortBy) {
        switch (sortBy) {
            case "end_date":
                if (!mCarItemsFiltered.equals(null))
                {    Collections.sort(mCarItemsFiltered, (l1, l2) -> Integer.compare(l1.getAuctionInfo().getEndDate(), l2.getAuctionInfo().getEndDate()));}
                Collections.sort(mCarItems, (l1, l2) -> Integer.compare(l1.getAuctionInfo().getEndDate(), l2.getAuctionInfo().getEndDate()));
                break;
            case "price":
                if (!mCarItemsFiltered.equals(null))
                { Collections.sort(mCarItemsFiltered, (l1, l2) -> Integer.compare(l1.getAuctionInfo().getCurrentPrice(), l2.getAuctionInfo().getCurrentPrice()));}
                Collections.sort(mCarItems, (l1, l2) -> Integer.compare(l1.getAuctionInfo().getCurrentPrice(), l2.getAuctionInfo().getCurrentPrice()));
                break;
            case "year":
                if (!mCarItemsFiltered.equals(null))
                { Collections.sort(mCarItemsFiltered, (l1, l2) -> Integer.compare(l1.getYear(), l2.getYear()));}
                Collections.sort(mCarItems, (l1, l2) -> Integer.compare(l1.getYear(), l2.getYear()));
                break;

        }
    }

    private void makeGetRequest() {
        if (mApiResponse == null) {
            mUrl = getString(R.string.get_cars);
            ServiceFactory.getData(mUrl, this, R.id.rv_cars_main);
        } else {
            Uri.Builder builder = new Uri.Builder();
            builder.scheme("http")
                    .authority(getString(R.string.base_address))
                    .appendPath("v2")
                    .appendPath("carsonline")
                    .appendQueryParameter("Ticks", String.valueOf(mApiResponse.getTicks()))
                    .appendQueryParameter("RefreshInterval", String.valueOf(mApiResponse.getRefreshInterval()));
            mUrl = builder.build().toString();
            ServiceFactory.getData(mUrl, this, R.id.rv_cars_main);
        }
    }

    @Override
    public void onSuccess(Object object) {
        updateRecyclerView(object);
    }

    @Override
    public void onFailure(Exception error, int id) {

    }

    private void updateRecyclerView(Object object) {
        try {
            if (mCarsRecyclerViewAdapter.getItemCount() == 0) {
                //LOAD LIST FOR THE FIRST TIME
                JSONObject result = new JSONObject(object.toString());
                Gson gson = new Gson();
                mApiResponse = gson.fromJson(result.toString(), new TypeToken<ApiResponse>() {
                }.getType());
                mCarItems = gson.fromJson(result.get("Cars").toString(), new TypeToken<ArrayList<CarItem>>() {
                }.getType());
                Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                    mCarsRecyclerViewAdapter = new CarsRecyclerViewAdapter(getContext(), mCarItems, mCarsRecyclerViewAdapter.isLinearLayout());
                    mCarsRecyclerView.setAdapter(mCarsRecyclerViewAdapter);
                    mCarsProgressBar.setVisibility(View.GONE);
                });
                mCarModels.add(PROMPT_STRING_FILTER_SPINNER);
                for (int i = 0; i < mCarItems.size(); i++) {
                    if (!mCarModels.contains(mCarItems.get(i).getModelEn())) {
                        mCarModels.add(mCarItems.get(i).getModelEn());
                    }
                }
            } else {
                //LOAD UPDATES ONLY
                JSONObject result = new JSONObject(object.toString());
                Gson gson = new Gson();
                mApiResponse = gson.fromJson(result.toString(), new TypeToken<ApiResponse>() {
                }.getType());
                mCarItems = gson.fromJson(result.get("Cars").toString(), new TypeToken<ArrayList<CarItem>>() {
                }.getType());
                Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                    mCarsRecyclerViewAdapter.addAll(mCarItems);
                    mCarsProgressBar.setVisibility(View.GONE);

                });
            }
        } catch (Exception error) {
            Log.e(getString(R.string.TAG), "HomeFragment 2: " + error.toString());
        }

    }

    private void initialize() {
        //noinspection unchecked
        savePrefs = new SavePrefs(Objects.requireNonNull(getContext()), CarsList.class);

        mCarsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mCarsProgressBar.setVisibility(View.VISIBLE);

        mCarItems = new ArrayList<>();

        mCarModels = new ArrayList<>();

        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
            mCarsRecyclerViewAdapter = new CarsRecyclerViewAdapter(getContext(), mCarItems, true);
            mCarsRecyclerView.setAdapter(mCarsRecyclerViewAdapter);
//                mCarsProgressBar.setVisibility(View.GONE);
        });
        ((AppCompatActivity) (Objects.requireNonNull(getActivity()))).setSupportActionBar(mToolbar);
        Objects.requireNonNull(((AppCompatActivity) (getActivity())).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(((AppCompatActivity) (getActivity())).getSupportActionBar()).setDisplayShowHomeEnabled(true);
        Objects.requireNonNull(((AppCompatActivity) (getActivity())).getSupportActionBar()).setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationOnClickListener(v -> {
            Intent intent = new Intent(getContext(), HomeActivity.class);
            startActivity(intent);
            getActivity().finish();
        });
    }

    public void setRecyclerViewLayoutManager(String LayoutManager) {
        int scrollPosition = 0;
        // If a layout manager has already been set, get current scroll position.
        if (mCarsRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mCarsRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        RecyclerView.LayoutManager mLayoutManager;
        switch (LayoutManager) {
            case "grid":
                mCarsRecyclerViewAdapter.setLinearLayout(false);
                mLayoutManager = new GridLayoutManager(getActivity(), 2);
                break;
            case "linear":
                mCarsRecyclerViewAdapter.setLinearLayout(true);
                mLayoutManager = new LinearLayoutManager(getActivity());
                break;
            default:
                mLayoutManager = new GridLayoutManager(getActivity(), 2);
        }
        mCarsRecyclerView.setLayoutManager(mLayoutManager);
        mCarsRecyclerView.setAdapter(mCarsRecyclerViewAdapter);
        mCarsRecyclerView.scrollToPosition(scrollPosition);
    }

}

